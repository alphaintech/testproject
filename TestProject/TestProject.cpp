// TestProject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	// Create black empty images
	Mat image = Mat::zeros(400, 400, CV_8UC3);

	// Draw a rectangle ( 5th argument is not -ve)
	rectangle(image, Point(15, 20), Point(70, 50), Scalar(255, 255, 255), 1, 8);
	//imshow("Image1", image);

	// Draw a filled rectangle ( 5th argument is -ve)
	rectangle(image, Point(100, 120), Point(150, 170), Scalar(100, 155, 25), CV_FILLED, 8);

	// Draw a circle 
	circle(image, Point(200, 200), 32.0, Scalar(0, 0, 255), 1, 8);

	imshow("Image", image);

	waitKey(0);
	return 0;
}

